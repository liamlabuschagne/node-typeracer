const textBox = document.querySelector("#text-box");
const inputBox = document.querySelector("#input-box");
const messageBox = document.querySelector(".game .message-box");
const server = "http://192.168.1.93:3000";
let textToType = "";
let words = [];
let wordIndex = 0;
let lastErrorCharacterIndex = 0;
let typedText = "";
let errors = 0;
let ms = 0;
let userId = -1;
let timer;

function renderColorOnTextPerCharFast(){
    let displayText = "";
    let inputText = typedText+inputBox.value;
    let lastWasWrong = false;
    let lastWasRight = false;
    
    for(let i = 0; i < textToType.length; i++){
        if(inputText.length-1 >= i && textToType[i] == inputText[i]){
            if(lastWasWrong){
                displayText+="</span><span class='green'>"+textToType[i];
            }
            else if(lastWasRight){
                displayText+=textToType[i];
            }
            else {
                displayText+="<span class='green'>"+textToType[i];
            }
            lastWasWrong = false;
            lastWasRight = true;
        }
        else if(inputText.length-1 >= i && textToType[i] != inputText[i]){
            
            if(i > lastErrorCharacterIndex){
                errors++;
                lastErrorCharacterIndex = i;
            }
            
            if(lastWasWrong){
                displayText+=textToType[i];
            }
            else if(lastWasRight){
                displayText+="</span><span class='red'>"+textToType[i];
            }
            else {
                displayText+="<span class='red'>"+textToType[i];
            }
            lastWasWrong = true;
            lastWasRight = false;
        }
        else {
            displayText+="</span>"+textToType[i];
        }
    }
    
    textBox.innerHTML = displayText;
}

function init(){
    words = textToType.split(" ");
    inputBox.value = "";
    renderColorOnTextPerCharFast();
    inputBox.focus();
    timer = setInterval(tick,10);
    document.querySelector(".menu").parentElement.removeChild(document.querySelector(".menu"));
    document.querySelector(".game").style.visibility = "visible";
}

function checkGameOver(){
    if(textToType == typedText.substr(0,typedText.length-1)){
        
        let minutes = (ms/100/60).toFixed(2);
        let wordsPerMinute = (words.length/minutes).toFixed(2);
        let accuracy = (100-(errors/textToType.length)*100).toFixed(2);
        
        inputBox.parentElement.removeChild(inputBox);
        
        messageBox.innerHTML+="<br>Minutes: "+minutes+"<br>";
        messageBox.innerHTML+="Words: "+words.length+"<br>";
        messageBox.innerHTML+="Words per minute: "+wordsPerMinute+"<br>";
        messageBox.innerHTML+="Errors: "+errors+"<br>";
        messageBox.innerHTML+="Accuracy: "+accuracy+"%";
        
        clearInterval(timer);
        
        // Get placing
    fetch(server+"/done/"+userId).then((response)=>{
            return response.json();  
        }).then((response2)=>{
            console.log(response2);
            if(response2.error){
                messageBox.innerHTML += "<br>"+response2.error;
            }else {
                messageBox.innerHTML += "<br>"+response2;
            }
        });
    }
}

function checkCurrentWord(){
    if(inputBox.value == words[wordIndex]+" "){
        typedText+=inputBox.value;
        inputBox.value = "";
        wordIndex++;
    }
}

function tick(){
    ms++;
    checkCurrentWord();
    renderColorOnTextPerCharFast();
    checkGameOver();
}

function getText(){
    fetch(server+'/text/'+userId).then((response)=>{
        return response.json();
    }).then((response2)=>{
        if(response2.text){
            textToType = response2.text;
            init();
        }else {
            setTimeout(getText,response2.timeRemaining*1000);
        }
    });
}

function displayPlayers(){
    fetch(server+'/getnames').then((response)=>{
        return response.json();
    }).then((response2)=>{
        let displayText = "<h2>Players in game:</h2>";
        for(let i = 0; i < response2.length; i++){
            displayText+="<p class='player-name'>"+response2[i]+"</p>";
        }
        document.querySelector(".menu .message-box").innerHTML = displayText;
    });
    if(textToType == ""){
        setTimeout(displayPlayers,1000);  
    }
}

function register(name){
    fetch(server+'/newuser/'+name).then((response)=>{
        return response.json();
    }).then((response2)=>{
        if(response2.error){
            document.querySelector(".menu .message-box").textContent = response2.error;
        }else {
            document.querySelector(".menu .message-box").textContent = "Registered!";
            userId = response2;
            document.querySelector(".choose-name").parentElement.removeChild(document.querySelector(".choose-name"));
            displayPlayers();
            getText();
        }
    });  
}

document.querySelector("#register").onclick = ()=>{
    let name = document.querySelector('#name').value;
    register(name);
}