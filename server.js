const http = require('http');
const fs = require('fs');

const hostname = '192.168.15.102';
const port = 3000;

let players = [];
let finished = [];

let playersReady = 0;
let textToType = "";
let timeToStart = 10;

function isEmpty(value){
    return (value == undefined ? true : false) || (value == "");
}

function chooseText(textPaths){
    let text = textPaths[Math.floor(Math.random()*(textPaths.length))];
    fs.readFile('./texts/'+text,'utf8',(err, data) => {
        if (err) throw err;
        data = data.toString().split('\n\n');
        let selected = Math.floor(Math.random()*data.length);
        while(data[selected].length < 150 || data[selected].length > 300){
            selected = Math.floor(Math.random()*data.length);
        }
        textToType = data[selected];
        console.log("Using a text from: "+text);
    });
}

function start(){
    let dec = 1;
    setInterval(()=>{
        timeToStart-=dec;
        if(timeToStart == 0){
            dec = 0;
        }
    },1000);
}

chooseText(['camel.txt','gingerbreadman.txt','thethreelittlepigs.txt']);

const server = http.createServer((req, res) => {
    res.writeHead(200,{'Access-Control-Allow-Origin':'*','Content-Type':'application/json'});
    var myObj = {};
    
    let requestData = req.url.split("/");
    let command = requestData[1];
    let value1 = requestData[2];
    if(!isEmpty(command)){
        
        
        // Handling creating new users for the current game
        if(command == "newuser"){
            if(timeToStart == 0){
                myObj = {error:"Game has already started, please wait for the next game."}
            }
            else if(!isEmpty(value1)){
                let usernameTaken = players.indexOf(value1) != -1;
                if(!usernameTaken){
                    myObj = (players.push(requestData[2]))-1; // Push the name, set userid to the index
                    // Check if timer should start
                    if(players.length >= 2){
                        start();
                    }
                }else if(usernameTaken){
                    myObj = {error:"Username taken."};
                }
            }
            else {
                myObj = {error:"Username empty."};
            }
        }
        
        // Retrieving a list of names
        if(command == "getnames"){
            myObj = players;
        }
        
        // Submiting as finished
        if(command == "done" && !isEmpty(value1)){
            if(value1 > players.length-1 || value1 < 0){
                myObj = {error:"Invalid ID."};
            }else {
                let placing = finished.push(value1);
                myObj = "You came: "+placing;
                if(placing == 1){
                    myObj+="st";
                }else if(placing == 2){
                    myObj+="nd";
                }else {
                    myObj+="rd";
                }
            }
        }
        
        // Get text
        if(command == "text" && !isEmpty(value1)){
            if(players[value1]){
                if(timeToStart == 0){
                    myObj = {text:textToType};
                }else {
                    myObj = {timeRemaining:timeToStart}
                    //myObj = "Please wait another: "+Math.floor(timeToStart/60)+" mins and "+(timeToStart-Math.floor(timeToStart/60)*60)+" secs";
                }
            }else {
                myObj = {error:"Please register first."};
            }
        }
    }
    
    res.end(JSON.stringify(myObj));
});

server.listen(port, hostname);
